package quartz;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.util.Date;

/**
 * Created by PK on 10/9/2015.
 */
public class QuartzDemo {

    public static void main(String[] args) throws SchedulerException, InterruptedException {



        StdSchedulerFactory factory = new StdSchedulerFactory();
        Scheduler scheduler = factory.getScheduler();

        JobDetail job1 = JobBuilder.newJob(HelloJob.class)
                .withIdentity("job 1", "group 1")
                .build();

        JobDetail job2 = JobBuilder.newJob(HelloJob.class)
                .withIdentity("job 2", "group 1")
                .build();

        Date date = new Date();
        Date runTime = DateBuilder.evenMinuteDate(date);

        Trigger trigger1 = TriggerBuilder.newTrigger()
                .withIdentity("trigger 1", "group 1")
                .startAt(runTime)
                .build();

        Trigger trigger2 = TriggerBuilder.newTrigger()
                .withIdentity("trigger 2", "group 2")
//                .withSchedule(CronScheduleBuilder.cronSchedule("13 1/4 * * * ?"))
                .withSchedule(CronScheduleBuilder.cronSchedule("* * * * * ?"))
                .build();

        Trigger trigger3 = TriggerBuilder.newTrigger()
                .withIdentity("trigger 2", "group 2")
                .withSchedule(CronScheduleBuilder.cronSchedule("* * * * * ?"))
                .startAt(DateBuilder.futureDate(5, DateBuilder.IntervalUnit.SECOND))
                .build();

        Trigger trigger4 = TriggerBuilder.newTrigger()
                .withIdentity("trigger 2", "group 2")
                .withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(2).withRepeatCount(3))
//                .withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(2).repeatForever())
//                .startAt(DateBuilder.futureDate(5, DateBuilder.IntervalUnit.SECOND))
                .build();

        scheduler.scheduleJob(job1, trigger4);
        scheduler.start();

//        int minutes = 4;
//        Thread.sleep(minutes*60*1000);

        Thread.sleep(10*1000);

        scheduler.shutdown(true);
    }
}
