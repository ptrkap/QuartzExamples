package quartz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.io.File;

import static org.slf4j.LoggerFactory.*;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Created by PK on 10/9/2015.
 */
public class HelloJob implements Job {

    private static org.slf4j.Logger logger =  getLogger(HelloJob.class);
//    private static org.slf4j.Logger logger =  org.apache.log4j.spi.LoggerFactory.getLogger(HelloJob.class);

    private static int counter = 0;

    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        String newDirPath = System.getProperty("newdir.path");
        String fileSeparator = System.getProperty("file.separator");
        String path = newDirPath + fileSeparator + (counter++);
        logger.info("Creating directory " + path);
        logger.debug("This is debug level log");
        logger.error("This is error level log");
        new File(path).mkdirs();
    }
}
